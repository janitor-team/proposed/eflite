dnl Much of this code stolen from Festival Lite's configure.in

AC_INIT(es.h)

AC_PROG_CC

if test "x$GCC" = "xyes"; then
	CFLAGS="$CFLAGS -Wall"
fi

AC_CHECK_LIB(socket,connect)
AC_SEARCH_LIBS(pthread_create,pthread c_r)

AC_ARG_VAR(flite_dir, Prefix for Festival Lite files)

dnl If flite_dir was not specified, then try to guess a value
if test "x$flite_dir" = x; then
  if test -f /usr/local/lib/libflite.a; then
    flite_dir=/usr/local
  elif test -f /usr/lib/libflite.a; then
    flite_dir=/usr
  elif test -f /opt/flite/lib/libflite.a; then
    flite_dir=/opt
  elif test -f $HOME/flite-1.2-release/lib/libflite.a; then
    flite_dir=$HOME/flite-1.2-release
  elif test -f $HOME/flite-1.1-release/lib/libflite.a; then
    flite_dir=$HOME/flite-1.1-release
  elif test -f /usr/src/flite-1.1-release/lib/libflite.a; then
    flite_dir=/usr/src/flite-1.1-release;
  elif test -f /usr/local/src/flite-1.1-release/lib/libflite.a; then
    flite_dir=/usr/local/src/flite-1.1-release;
  elif test -f /usr/local/lib/libflite.so; then
    flite_dir=/usr/local
  elif test -f /usr/lib/libflite.so; then
    flite_dir=/usr
  elif test -f /opt/flite/lib/libflite.so; then
    flite_dir=/opt
  elif test -f $HOME/flite-1.2-release/lib/libflite.so; then
    flite_dir=$HOME/flite-1.2-release
  elif test -f $HOME/flite-1.1-release/lib/libflite.so; then
    flite_dir=$HOME/flite-1.1-release
  elif test -f /usr/src/flite-1.1-release/lib/libflite.so; then
    flite_dir=/usr/src/flite-1.1-release;
  elif test -f /usr/local/src/flite-1.1-release/lib/libflite.so; then
    flite_dir=/usr/local/src/flite-1.1-release;
  else
    AC_MSG_ERROR(Cannot locate libflite.a.  Please specify flite_dir explicitly (see INSTALL).  If you do not have FLite headers and libraries available, then compile FLite and re-run configure.)
    fi
  fi

echo Using $flite_dir as FLite prefix.

AC_ARG_VAR(flite_include_dir, Directory of Festival Lite include files)

dnl If flite_include_dir was not specified, then try to guess a value
if test "x$flite_include_dir" = x; then
  if test -f $flite_dir/include/flite/cst_voice.h; then
    flite_include_dir=$flite_dir/include/flite
  elif test -f $flite_dir/include/cst_voice.h; then
    flite_include_dir=$flite_dir/include
  else
    AC_MSG_ERROR(Cannot locate cst_voice.h.  Please specify flite_include_dir explicitly.)
  fi
fi

echo Using $flite_include_dir as FLite include directory.

dnl
dnl determine audio type or use none if none supported on this platform
dnl
AUDIODRIVER=none
AC_CHECK_HEADER(sys/soundcard.h,
              [AUDIODRIVER="oss"
               AUDIODEFS=-DCST_AUDIO_LINUX])
AC_CHECK_HEADER(machine/soundcard.h,
              [AUDIODRIVER="oss"
               AUDIODEFS=-DCST_AUDIO_FREEBSD])
AC_CHECK_HEADER(sys/audioio.h,
              [AUDIODRIVER="sun"
               AUDIODEFS=-DCST_AUDIO_SUNOS])
AC_CHECK_HEADER(alsa/asoundlib.h,
              [AUDIODRIVER="alsa"
	       AUDIODEFS=-DCST_AUDIO_ALSA
		                  AUDIOLIBS=-lasound])
AC_CHECK_HEADER(mmsystem.h,
	      [AUDIODRIVER="wince"
	       AUDIODEFS=-DCST_AUDIO_WINCE
	       AUDIOLIBS=-lwinmm])


dnl
dnl allow the user to override the one detected above
dnl
AC_ARG_WITH( audio,
	[  --with-audio          with specific audio support (none linux freebsd etc) ],
        AUDIODRIVER=$with_audio )

if test "x$AUDIODEFS" = x; then
    case "$AUDIODRIVER" in
	linux|oss)
	    AUDIODRIVER=oss
	    AUDIODEFS=-DCST_AUDIO_LINUX
	    ;;
	*bsd)
	    AUDIODRIVER=oss
	    AUDIODEFS=-DCST_AUDIO_FREEBSD
	    ;;
	qnx)
	    AUDIODRIVER=alsa
	    AUDIODEFS=-DCST_AUDIO_QNX
	    ;;
	none)
	    AUDIODEFS=-DCST_AUDIO_NONE
	    ;;
    esac
fi
AC_SUBST(AUDIODRIVER)
AC_SUBST(AUDIODEFS)
AC_SUBST(AUDIOLIBS)

dnl
dnl language/lexicon/voice parameters (we don't really support multiple
dnl ones yet, but its included as a start
dnl
AC_ARG_WITH( lang, [  --with-lang           with language ],
        FL_LANG=$with_lang )
if test "x$with_lang" = "x"; then
        FL_LANG="usenglish"
fi
AC_SUBST(FL_LANG)

AC_ARG_WITH( vox, [  --with-vox            with vox ],
        FL_VOX=$with_vox )
if test "x$with_vox" = "x"; then
        FL_VOX="cmu_us_kal"
fi
AC_SUBST(FL_VOX)

AC_ARG_WITH( lex, [  --with-lex            with lexicon ],
        FL_LEX=$with_lex )
if test "x$with_lex" = "x"; then
        FL_LEX="cmulex"
fi
AC_SUBST(FL_LEX)
AC_SUBST(LIBS)

AC_OUTPUT(Makefile)
